#!/bin/bash
# script to test probtrackx_gpu
# The test utilizes a script by Prof. Chris Rorden
# 08 Dec 2022 K. Nemoto

cd ~/git
if [[ ! -d gpu_test ]]; then
  git clone https://github.com/neurolabusc/gpu_test.git
else
  cd gpu_test
  git pull
fi

~/git/gpu_test/btest/runme_gpu.sh

exit

