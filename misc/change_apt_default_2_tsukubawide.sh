#!/bin/bash

# script to change repository location from
# http://jp.archive.ubuntu.com/ubuntu
# to
# http://ftp.tsukuba.wide.ad.jp/Linux/ubuntu/

sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
sudo sed -i 's@http://jp.archive.ubuntu.com/ubuntu/@http://ftp.tsukuba.wide.ad.jp/Linux/ubuntu/@' /etc/apt/sources.list

sudo apt update

exit

