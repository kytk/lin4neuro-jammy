#!/bin/bash

# Lin4Neuro building script for AWS-based Ubuntu 22.04 (Jammy)
# 12 Jan 2025 K. Nemoto

log_and_exec() {
    echo "$1"
    shift
    "$@"
}

setup_locale() {
    local lang="$1"
    if [ "$lang" == "Japanese" ]; then
        log_and_exec "Setting up Japanese locale" sudo apt-get install -y language-pack-ja manpages-ja
        sudo update-locale LANG=ja_JP.UTF-8
        sudo sed -i 's|http://us.|http://ja.|g' /etc/apt/sources.list
    fi
}

setup_neurodebian_repo() {
    local base_url="$1"
    cat << EOS > neurodebian.sources.list
deb [arch=amd64 signed-by=/usr/share/keyrings/neurodebian.gpg] ${base_url} data main contrib non-free
deb [arch=amd64 signed-by=/usr/share/keyrings/neurodebian.gpg] ${base_url} $(lsb_release -cs) main contrib non-free
EOS
    sudo mv neurodebian.sources.list /etc/apt/sources.list.d/
}

download_and_install() {
    local url="$1"
    local filename=$(basename "$url")
    wget "$url"
    sudo apt install -y "./$filename"
    rm "$filename"
}

##### STEP 0. PREPARATION ######################################################
LANG=C
log_and_exec "Updating system" sudo apt-get update && sudo apt-get upgrade -y
log_and_exec "Installing wget and gnupg" sudo apt-get install -y wget gnupg

log=$(date +%Y%m%d%H%M%S)-aws.log
exec &> >(tee -a "$log")

echo "Begin making Lin4Neuro."
select lang in "English" "Japanese" "quit"; do
    case "$lang" in
        "Japanese")
            setup_locale "Japanese"
            setup_neurodebian_repo "http://neuroimaging.sakura.ne.jp/neurodebian"
            break
            ;;
        "English")
            setup_neurodebian_repo "http://neuro.debian.net/debian"
            break
            ;;
        "quit")
            echo "quit."
            exit 0
            ;;
    esac
done

log_and_exec "Setting up Neurodebian signature" wget -O- http://neuro.debian.net/_static/neuro.debian.net.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/neurodebian.gpg
log_and_exec "Updating system" sudo apt-get update

##### STEP 1. Install XFCE and utilities #######################################
log_and_exec "Removing gdm3 and wayland" sudo apt-get -y purge gdm3 xwayland
log_and_exec "Installing XFCE" sudo apt-get install -y xfce4 xfce4-terminal xfce4-indicator-plugin xfce4-clipman xfce4-clipman-plugin xfce4-statusnotifier-plugin xfce4-power-manager-plugins xfce4-screenshooter lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings shimmer-themes xinit build-essential dkms thunar-archive-plugin file-roller gawk xdg-utils

# Remove gdm3 and wayland
sudo apt-get -y purge gdm3 xwayland


# Install XFCE
echo "Install XFCE"
sudo apt-get install -y xfce4 xfce4-terminal xfce4-indicator-plugin  \
 xfce4-clipman xfce4-clipman-plugin xfce4-statusnotifier-plugin  \
 xfce4-power-manager-plugins xfce4-screenshooter \
 lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings \
 shimmer-themes xinit build-essential  \
 dkms thunar-archive-plugin file-roller gawk xdg-utils 


##### STEP 2. INSTALL LIN4NEURO ENVIRONMENTS ###################################

# Path of the lin4neuro parts
base_path=$PWD/lin4neuro-parts

# Directories
sudo mkdir /etc/skel/{Desktop,Documents,Downloads,Pictures,Public,Templates,Videos,bin}

# Icons
mkdir -p ~/.local/share
cp -r "${base_path}"/local/share/icons ~/.local/share/
sudo mkdir -p /etc/skel/.local/share
sudo cp -r "${base_path}"/local/share/icons /etc/skel/.local/share/

# Customized menu
mkdir -p ~/.config/menus
cp "${base_path}"/config/menus/xfce-applications.menu ~/.config/menus
sudo mkdir -p /etc/skel/.config/menus
sudo cp "${base_path}"/config/menus/xfce-applications.menu \
 /etc/skel/.config/menus

# Cusotmized panel, dsktop, and theme
cp -r "${base_path}"/config/xfce4 ~/.config
sudo cp -r "${base_path}"/config/xfce4 /etc/skel/.config

# Desktop files
cp -r "${base_path}"/local/share/applications ~/.local/share/
sudo cp -r "${base_path}"/local/share/applications /etc/skel/.local/share/

# Neuroimaging.directory
mkdir -p ~/.local/share/desktop-directories
cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory ~/.local/share/desktop-directories
sudo mkdir -p /etc/skel/.local/share/desktop-directories
sudo cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory /etc/skel/.local/share/desktop-directories

# Background image (and remove unnecessary image file)
sudo cp "${base_path}"/backgrounds/deep_ocean.png /usr/share/backgrounds
cd /usr/share/backgrounds/xfce
sudo rm *.jpg *.png
sudo ln -s /usr/share/backgrounds/deep_ocean.png /usr/share/backgrounds/xfce/xfce-stripes.png
sudo ln -s /usr/share/backgrounds/deep_ocean.png /usr/share/backgrounds/xfce/xfce-verticals.png

# Modified lightdm-gtk-greeter.conf
sudo mkdir -p /usr/share/lightdm/lightdm-gtk-greeter.conf.d
sudo cp "${base_path}"/lightdm/lightdm-gtk-greeter.conf.d/01_ubuntu.conf /usr/share/lightdm/lightdm-gtk-greeter.conf.d

# Auto-login
sudo mkdir -p /usr/share/lightdm/lightdm.conf.d
sudo cp "${base_path}"/lightdm/lightdm.conf.d/10-ubuntu.conf \
 /usr/share/lightdm/lightdm.conf.d

# Network Manager
sudo cp "${base_path}"/etc/NetworkManager/conf.d/10-globally-managed-devices.conf /etc/NetworkManager/conf.d/10-globally-managed-devices.conf

# Clean packages
sudo apt-get -y autoremove

# GRUB customization to show GRUB on boot
sudo sed -i -e 's/GRUB_TIMEOUT_STYLE/#GRUB_TIMEOUT_STYLE/' /etc/default/grub
sudo sed -i -e 's/GRUB_TIMEOUT=0/GRUB_TIMEOUT=10/' /etc/default/grub
sudo update-grub


# alias
cat << EOS >> ~/.bash_aliases

#alias for xdg-open
alias open='xdg-open &> /dev/null'

EOS


#### Ubuntu to Lin4Neuro (if Ubuntu is installed beforehand)
# Delete ubuntu-desktop
sudo apt-get purge -y nautilus gnome-power-manager \
  gnome-bluetooth gnome-desktop* gnome-session* \
  gnome-user* gnome-shell-common gnome-screenshot*
sudo apt-get install -y gnome-software gnome-system-tools eog evince
sudo apt-get purge -y gnome-terminal* tracker tracker-extract tracker-miner-fs
 
# Clean packages
sudo apt-get -y autoremove

# Remove unnecessary configuration files
dpkg -l | awk '/^rc/ {print $2}' | xargs sudo dpkg --purge

# locale-related settings
if [ $lang == "Japanese" ] ; then
  sed -i -e 's/デスクトップ/Desktop/' -e 's/ダウンロード/Downloads/' \
    -e 's/テンプレート/Templates/' -e 's/公開/Public/' \
    -e 's/ドキュメント/Documents/' -e 's/ミュージック/Music/' \
    -e 's/ピクチャ/Pictures/' -e 's/ビデオ/Videos/' ~/.config/user-dirs.dirs
  echo "ja_JP" > ~/.config/user-dirs.locale
  cd $HOME
  rmdir ダウンロード テンプレート デスクトップ ドキュメント \
        ビデオ ピクチャ ミュージック 公開
fi
####

##### Install utilities #####
log_and_exec "Installing network-manager-gnome" sudo apt-get install -y --no-install-recommends network-manager-gnome
log_and_exec "Installing python-related packages" sudo apt-get install -y python3-pip python3-venv python3-dev python3-tk python3-gpg
log_and_exec "Installing Jupyter and other Python packages" sudo -H pip3 install jupyter notebook bash_kernel numpy matplotlib pandas scikit-learn scipy seaborn
log_and_exec "Installing other utilities" sudo apt-get install -y at-spi2-core bc byobu curl wget dc default-jre evince exfat-fuse gedit mousepad gnome-system-monitor gnome-system-tools gparted imagemagick rename ntp system-config-printer-gnome tree unzip update-manager vim alsa-base wajig zip ntp tcsh baobab xterm bleachbit libopenblas-base cups apturl dmz-cursor-theme chntpw gddrescue p7zip-full gnupg eog meld software-properties-common fonts-noto mupdf mupdf-tools pigz ristretto pinta

# xrdp
sudo apt install -y xrdp
sudo systemctl enable xrdp

# Install network-manager-gnome
sudo apt-get install -y --no-install-recommends network-manager-gnome 

# Install python-related packages
sudo apt-get install -y python3-pip python3-venv python3-dev python3-tk \
     python3-gpg
sudo -H pip3 install jupyter notebook bash_kernel
sudo -H python3 -m bash_kernel.install
sudo -H pip3 install numpy matplotlib pandas scikit-learn scipy seaborn
sudo apt-get install -y pkg-config libopenblas-dev liblapack-dev  \
 libhdf5-serial-dev graphviz 


download_and_install "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
log_and_exec "Setting Google Chrome as default for xdg-mime" xdg-mime default google-chrome.desktop text/html


# Install utilities
sudo apt-get install -y at-spi2-core bc byobu curl wget dc \
 default-jre evince exfat-fuse gedit mousepad \
 gnome-system-monitor gnome-system-tools gparted  \
 imagemagick rename ntp system-config-printer-gnome  \
 tree unzip update-manager vim alsa-base wajig zip ntp tcsh baobab xterm \
 bleachbit libopenblas-base cups apturl dmz-cursor-theme \
 chntpw gddrescue p7zip-full gnupg eog meld \
 software-properties-common fonts-noto mupdf mupdf-tools pigz \
 ristretto pinta

# Workaround for system-config-samba
sudo touch /etc/libuser.conf
# Vim settings
sudo cp /usr/share/vim/vimrc /etc/skel/.vimrc
sudo sed -i -e 's/"set background=dark/set background=dark/' /etc/skel/.vimrc

# Install libreoffice with language locale
if [ $lang == "English" ] ; then
  #English-dependent packages
  #echo "Installation of firefox"
  #sudo apt-get install -y firefox firefox-locale-en
  echo "Installation of libreoffice"
  sudo apt-get install -y libreoffice libreoffice-help-en-us
elif [ $lang == "Japanese" ] ; then
  #Japanese-dependent environment
  echo "Installation of Japanese-related packages"
  sudo apt-get install -y fcitx fcitx-mozc fcitx-config-gtk  \
              unar nkf im-config language-pack-ja language-pack-gnome-ja 
  #sudo apt-get install -y firefox firefox-local-ja firefox-locale-en
  echo "Installation of libreoffice"
  sudo apt-get install -y libreoffice libreoffice-l10n-ja \
              libreoffice-help-ja
  # Change name of directories to English
  LANG=C xdg-user-dirs-update --force
  im-config -n fcitx
fi

# (optional) latest Libreoffice
#Uncomment the following three lines to use the latest libreoffice
sudo add-apt-repository -y ppa:libreoffice/ppa
sudo apt-get update
sudo apt-get -y dist-upgrade


##### STEP 0. PREPARATION ######################################################
# Log
log=$(date +%Y%m%d%H%M%S)-vm2.log
exec &> >(tee -a "$log")


# Working directory
wd=$PWD # should be lin4neuro-jammy


# Settings for Japanese
if [ $LANG == "ja_JP.UTF-8" ]; then
  LANG=C xdg-user-dirs-update --force
  cd $HOME
  if [ -d ダウンロード ]; then
    rm -rf ダウンロード テンプレート デスクトップ ドキュメント ビデオ \
           ピクチャ ミュージック 公開
  fi
  im-config -n fcitx
fi


##### STEP 1. INSTALL NEUROIMAGING SOFTWARE PACKAGES USING APT #################

# Curl (reassure to install)
sudo apt-get install -y curl


echo "Install neuroimaging-related software packages"


#DCMTK
echo "Install DCMTK"
sudo apt-get install -y dcmtk


#Octave
echo "Install Octave"
sudo apt-get install -y octave
sudo pip3 install octave_kernel


#R
echo "Install R"
sudo apt-get install -y r-base


#R (cloud.r-project.org; for version 4.1)
#sudo apt-key adv --keyserver keyserver.ubuntu.com \
#     --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
#
#echo "Install R using cran.rstudio.com repository"
#
#if ! grep -q 'rstudio' /etc/apt/sources.list; then
#  sudo add-apt-repository \
#  'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran40/'
#fi
#sudo apt-get -y update

echo -e "Install python libraries \n"
#Installation of python libraries for machine learning
sudo -H pip3 install jupyter notebook numpy scipy opencv-python python-dateutil
sudo -H pip3 install cmake matplotlib pyyaml h5py pydot-ng pillow 
sudo -H pip3 install scikit-learn pandas seaborn
sudo -H pip3 install pydicom heudiconv dcm2bids gdcm


##### STEP 2. INSTALL NEUROIMAGING SOFTWARE PACKAGES MANUALLY  #################

# AlizaMS
echo "Install Aliza MS"

alizatgz='alizams-1.8.3_linux.tar.gz'

cd $HOME/Downloads

if [ ! -e ${alizatgz} ]; then
  curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${alizatgz}
fi

cd /usr/local

if [ -d alizams ]; then
  sudo rm -rf alizams
fi

sudo tar xvf $HOME/Downloads/${alizatgz}
sudo mv ${alizatgz%.tar.gz} alizams

if [ ! -e ~/.local/share/aaplications/alizams.desktop ]; then
  find $HOME -name 'alizams.desktop' 2>/dev/null -exec cp {} ~/.local/share/applications/ \;
fi

sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/alizams.desktop

if ! grep -q 'Aliza MS' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# Aliza MS' >> ~/.bash_aliases
  echo "alias alizams='/usr/local/alizams/alizams.sh'" >> ~/.bash_aliases
fi


# c3d
echo "Install c3d"
cd "$HOME"/Downloads

if [ ! -e 'c3d-1.0.0-Linux-x86_64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/c3d-1.0.0-Linux-x86_64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/c3d-1.0.0-Linux-x86_64.tar.gz
sudo mv c3d-1.0.0-Linux-x86_64 c3d

if ! grep -q 'c3d' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# c3d' >> ~/.bash_aliases
  echo 'export PATH=$PATH:/usr/local/c3d/bin' >> ~/.bash_aliases
  echo 'source /usr/local/c3d/share/bashcomp.sh' >> ~/.bash_aliases
fi


#Mango
echo "Install Mango"
cd $HOME/Downloads

if [ ! -e 'mango_unix.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mango_unix.zip
fi

cd /usr/local
sudo unzip ~/Downloads/mango_unix.zip

if ! grep -q 'Mango' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# Mango' >> ~/.bash_aliases
  echo 'export PATH=$PATH:/usr/local/Mango' >> ~/.bash_aliases
fi

mkdir -p ~/bin
cat << 'EOS' > ~/bin/mango
#!/bin/bash
/usr/local/Mango/jre7/bin/java -Djava.awt.headless=true -Xms64M -Xmx512M -XX:MaxDirectMemorySize=1536M -cp "/usr/local/Mango/Mango.jar" edu.uthscsa.ric.mango.MangoClient "$@"
EOS


#MRIcroGL
echo "Install MRIcroGL"
sudo apt-get -y install appmenu-gtk2-module

cd $HOME/Downloads

curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip

cd /usr/local
sudo unzip ~/Downloads/MRIcroGL_linux.zip

if ! grep -q 'MRIcroGL' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# MRIcroGL' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/MRIcroGL:$PATH' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/MRIcroGL/Resources:$PATH' >> ~/.bash_aliases
fi

# dcm2niix
echo "Install dcm2niix"
ver=v1.0.20241211

cd $HOME/Downloads

rm dcm2niix_lnx.zip
curl -OL https://github.com/rordenlab/dcm2niix/releases/download/${ver}/dcm2niix_lnx.zip

sudo mkdir -p /usr/local/dcm2niix
sudo unzip dcm2niix_lnx.zip -d /usr/local/dcm2niix

if ! grep -q '# dcm2niix' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# dcm2niix' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/dcm2niix:$PATH' >> ~/.bash_aliases
fi


# MRIcron
echo "Install MRIcron"
cd $HOME/Downloads

if [ ! -e 'MRIcron_linux.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcron_linux.zip
fi

cd /usr/local
sudo unzip ~/Downloads/MRIcron_linux.zip

sudo find /usr/local/mricron -name 'dcm2niix' -exec rm {} \;
 # Delete dcm2niix because this version is old

sudo find /usr/local/mricron -name '*.bat' -exec rm {} \;
 # Delete batch files since they are not available anymore

sudo find /usr/local/mricron -type d -exec chmod 755 {} \;
sudo find /usr/local/mricron/Resources -type f -exec chmod 644 {} \;
sudo chmod 755 /usr/local/mricron/Resources/pigz_mricron

if ! grep -q 'mricron' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# MRIcron' >> ~/.bash_aliases
  echo 'export PATH=$PATH:/usr/local/mricron' >> ~/.bash_aliases
fi


# ROBEX
echo "Install ROBEX"
cd $HOME/Downloads

if [ ! -e 'ROBEXv12.linux64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/ROBEXv12.linux64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/ROBEXv12.linux64.tar.gz
sudo chmod 755 ROBEX
cd ROBEX
sudo find -type f -exec chmod 644 {} \;
sudo chmod 755 ROBEX runROBEX.sh dat ref_vols

if ! grep -q 'ROBEX' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# ROBEX' >> ~/.bash_aliases
  echo 'export PATH=$PATH:/usr/local/ROBEX' >> ~/.bash_aliases
fi


# Surf-Ice
echo "Install Surf-Ice"
cd $HOME/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/surfice_linux.zip

cd /usr/local
sudo unzip ~/Downloads/surfice_linux.zip

cd Surf_Ice
sudo find . -type d -exec chmod 755 {} \;
sudo find . -type f -exec chmod 644 {} \;
sudo chmod 755 surfice*
sudo chmod 644 surfice_Linux_Installation.txt

if ! grep -q 'Surf_Ice' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# Surf_Ice' >> ~/.bash_aliases
  echo 'export PATH=$PATH:/usr/local/Surf_Ice' >> ~/.bash_aliases
fi


# Talairach daemon
echo "Install Talairach daemon"
sudo cp -r ${wd}/lin4neuro-parts/tdaemon /usr/local
if ! grep -q 'Talairach' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# Talairach daemon' >> ~/.bash_aliases
  echo "alias tdaemon='java -jar /usr/local/tdaemon/talairach.jar'" >> ~/.bash_aliases
fi


# VirtualMRI
echo "Install Virtual MRI"
cd $HOME/Downloads

if [ ! -e 'vmri.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/vmri.zip
fi

cd /usr/local
sudo unzip ~/Downloads/vmri.zip


##### STEP 3. POST-INSTALLATION TUNING #########################################

# Remove unnecessary files
sudo find / -name '__MACOSX' -exec rm -rf {} \;
sudo find / -name '.DS_Store' -exec rm -rf {} \;
sudo find / -name '._*' -exec rm {} \;
sudo find / -name 'Thumbs.db' -exec rm {} \;


# copy .bash_aliases and .profile to /etc/skel
sudo cp ~/.bash_aliases ~/.profile /etc/skel


# Change /bin/sh from dash to bash
echo "dash dash/sh boolean false" | sudo debconf-set-selections
sudo dpkg-reconfigure --frontend=noninteractive dash


echo "Finished!"

