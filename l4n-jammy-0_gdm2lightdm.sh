#!/bin/bash

# install lightdm first
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y lightdm

# select lightdm
echo "/usr/sbin/lightdm" | sudo tee /etc/X11/default-display-manager

# uninstall gdm3
sudo DEBIAN_FRONTEND=noninteractive apt-get remove gdm3

# reboot
sudo systemctl reboot
