#!/bin/bash
#Install Matlab R2022b Runtime

zipfile=MATLAB_Runtime_R2024b_Update_1_glnxa64.zip
ver=242

echo "Install Matlab R2024b (24.2) Runtime"
cd $HOME/Downloads

if [ ! -e $zipfile ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${zipfile}
fi

mkdir mcr_v${ver}
cd mcr_v${ver}
unzip ../${zipfile}

#Install to /usr/local/MATLAB/MCR/
sudo ./install -mode silent -agreeToLicense yes \
    -destinationFolder /usr/local/MATLAB/MCR/

echo "Finished!"

exit

