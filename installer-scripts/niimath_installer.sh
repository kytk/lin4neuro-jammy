#!/bin/bash
# install script for niimath on Ubuntu

# 12 Jan 2025 K. Nemoto

set -x

cd $HOME/Downloads

# remove (possible) previous versions
[[ -e niimath_lnx.zip ]] && rm niimath_lnx.zip

# Download the latest version
curl -fLO https://github.com/rordenlab/niimath/releases/latest/download/niimath_lnx.zip

sudo mkdir -p /usr/local/niimath
sudo unzip niimath_lnx.zip -d /usr/local/niimath

if ! grep -q '# niimath' ~/.bash_aliases; then
  echo '' >> ~/.bash_aliases
  echo '# niimath' >> ~/.bash_aliases
  echo 'export PATH=$PATH:/usr/local/niimath' >> ~/.bash_aliases
fi

exit

