#!/bin/bash
# install script for BIDSconvertR on Ubuntu

# 12 Dec 2024 K. Nemoto

set -x

#R studio
pushd $HOME/Downloads
curl -O https://download1.rstudio.org/electron/jammy/amd64/rstudio-2024.04.2-764-amd64.deb
sudo apt install ./rstudio-2024.04.2-764-amd64.deb
popd

# Install packages for devtools
sudo apt-get install -y libfreetype6-dev libfribidi-dev libharfbuzz-dev \
  git libxml2-dev make libfontconfig1-dev libicu-dev pandoc zlib1g-dev \
  libssl-dev libjpeg-dev libpng-dev libtiff-dev libgit2-dev \
  libcurl4-openssl-dev gfortran

#Installation of BIDSconvertR
cd $HOME/Downloads
cat << EOS > BIDSconvertR.R
install.packages("shiny")
install.packages("devtools")
devtools::install_github(repo = "bidsconvertr/bidsconvertr")
EOS

Rscript BIDSconvertR.R

