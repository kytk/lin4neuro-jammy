#!/bin/bash
# install script for dcm2niix on Ubuntu

# 03 Jan 2025 K. Nemoto

#set -x

ver=v1.0.20241211

cd $HOME/Downloads

# remove (possible) previous versions
[[ -e dcm2niix_lnx.zip ]] && rm dcm2niix_lnx.zip

# Download the latest version
curl -OL https://github.com/rordenlab/dcm2niix/releases/download/${ver}/dcm2niix_lnx.zip

sudo mkdir -p /usr/local/dcm2niix
sudo unzip -o dcm2niix_lnx.zip -d /usr/local/dcm2niix

grep '# dcm2niix' ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> ~/.bash_aliases
  echo '# dcm2niix' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/dcm2niix:$PATH' >> ~/.bash_aliases
fi

exit

