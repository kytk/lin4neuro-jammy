#!/bin/bash

alizadeb='alizams_1.9.10+git0.95d7909-1+1.1_amd64.deb'

cd $HOME/Downloads

if [ ! -e ${alizadeb} ]; then
  curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${alizadeb}
fi

sudo apt install ./${alizadeb}

if [ ! -e ~/.local/share/aaplications/alizams.desktop ]; then
  find $HOME -name 'alizams.desktop' 2>/dev/null -exec cp {} ~/.local/share/applications/ \;
fi

sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/alizams.desktop

echo "Finished!"
sleep 5
exit
