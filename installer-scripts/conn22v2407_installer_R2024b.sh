#!/bin/bash
#CONN22v2407 standalone installer

#Check MCR is installed
if [ ! -d /usr/local/MATLAB/MCR/R2024b ]; then
  echo "Matlab Compiler Runtime needs to be installed first!"
  ~/git/lin4neuro-jammy/installer-scripts/mcr_R2024b_installer.sh
fi

#Download CONN22v2407 standalone
echo "Download CONN22v2407 standalone"
cd $HOME/Downloads

if [ ! -e 'conn22v2407_standalone_jammy_R2024b.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/conn22v2407_standalone_jammy_R2024b.zip
fi

cd /usr/local
sudo unzip ~/Downloads/conn22v2407_standalone_jammy_R2024b.zip
sudo chown -R $USER:$USER /usr/local/conn22v2407_standalone
echo "-XX:ReservedCodeCacheSize=256m" > /usr/local/conn22v2407_standalone/java.opts

#Desktop entry
cat << EOS > ~/.local/share/applications/conn22v2407.desktop
[Desktop Entry]
Encoding=UTF-8
Name=CONN 22v2407
Exec=bash -c '/usr/local/conn22v2407_standalone/run_conn.sh /usr/local/MATLAB/MCR/R2024b'
Icon=conn.png
Type=Application
Terminal=true
Categories=Neuroimaging;
NoDisplay=false
EOS

#alias
grep conn22v2407 ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#conn22v2407 standalone' >> ~/.bash_aliases
    echo "alias conn='/usr/local/conn22v2407_standalone/run_conn.sh /usr/local/MATLAB/MCR/R2024b'" >> ~/.bash_aliases
fi

echo "Finished!"
sleep 5 
exit

