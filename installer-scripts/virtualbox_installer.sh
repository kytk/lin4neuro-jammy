#!/bin/bash

sudo sh -c "echo '' >> /etc/apt/sources.list"
sudo sh -c "echo '
deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian jammy contrib' >> /etc/apt/sources.list"

wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg

sudo apt-get update
sudo apt-get install -y virtualbox-7.0


