#!/bin/bash

archive='Slicer-5.6.1-linux-amd64.tar.gz'

echo "Install 3D-Slicer"
cd $HOME/Downloads

# Install prerequisite
# This is based on https://slicer.readthedocs.io/en/latest/user_guide/getting_started.html#linux

sudo apt-get install -y libpulse-dev libnss3 libglu1-mesa
sudo apt-get install -y --reinstall libxcb-xinerama0

if [ ! -e $archive ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/$archive
fi

cd /usr/local
#remove previous version
sudo rm -rf Slicer
sudo tar xvzf ~/Downloads/$archive
sudo mv ${archive%.tar.gz} Slicer
sudo chown $USER:$USER /usr/local/Slicer

grep Slicer ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#Slicer' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Slicer' >> ~/.bash_aliases
fi

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/3dslicer.desktop

echo "Finished!"
sleep 5
exit
