#!/bin/bash
#Install Matlab R2022b Runtime

zipfile=MATLAB_Runtime_R2022b_Update_10_glnxa64.zip

echo "Install Matlab R2022b (v913) Runtime"
cd $HOME/Downloads

if [ ! -e $zipfile ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${zipfile}
fi

mkdir mcr_v913
cd mcr_v913
unzip ../${zipfile}

#Install to /usr/local/MATLAB/MCR/v913
sudo ./install -mode silent -agreeToLicense yes \
    -destinationFolder /usr/local/MATLAB/MCR/

echo "Finished!"

exit

