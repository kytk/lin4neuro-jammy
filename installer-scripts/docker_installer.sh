#!/bin/bash
# install script for docker engine on Ubuntu
# based on the instruction below
# https://docs.docker.com/engine/install/ubuntu/
# https://docs.docker.com/engine/install/linux-postinstall/

# Uninstall old versions
sudo apt-get remove docker docker-engine docker.io containerd runc

# Install utilities to allow apt to use a repository over HTTPS
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg lsb-release

# Add Docker's Official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Set up stable repository
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg]  https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install Docker Engine
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin


# Manage Docker as a non-root user
sudo groupadd docker
sudo usermod -aG docker $USER

# Activate the changes to the groups
newgrp docker

# Add path for docker-compose and etc
echo '' >> ~/.bash_aliases
echo '# Docker-related scripts' >> ~/.bash_aliases
echo 'export PATH=$PATH:/usr/libexec/docker/cli-plugins/' >> ~/.bash_aliases


