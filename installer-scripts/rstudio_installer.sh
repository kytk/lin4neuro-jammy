#!/bin/bash
# install script for Rstudio on Ubuntu

# 12 Dec 2024 K. Nemoto

set -x

#R studio
pushd $HOME/Downloads
curl -O https://download1.rstudio.org/electron/jammy/amd64/rstudio-2024.04.2-764-amd64.deb
sudo apt install ./rstudio-2024.04.2-764-amd64.deb
popd

