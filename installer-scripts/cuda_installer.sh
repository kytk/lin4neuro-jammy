#!/bin/bash

# Install cuda drivers with open source
# Part of this script is based on the link below from NVIDIA
#https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#ubuntu

# 24 Feb 2025 K. Nemoto

# Fore debugging
set -x

# Delete previous public key
#sudo apt-key del 7fa2af80

# Install the new cuda-keyring package
cd ~/Downloads
[[ -e cuda-keyring_1.1-1_all.deb ]] && rm cuda-keyring_1.1-1_all.deb
if [[ ! -e /etc/apt/sources.list.d/cuda-ubuntu2204-x86_64.list ]]; then
  wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.1-1_all.deb
  sudo dpkg -i cuda-keyring_1.1-1_all.deb 
fi
sudo apt-get update

# Check if cuda-11-8 is set hold by apt-mark
apt-mark showhold | grep cuda-11-8 > /dev/null
[[ $? -eq 0 ]] && sudo apt-mark unhold cuda-11-8

# Install nvidia-driver-570-open
sudo apt-get -y install nvidia-driver-570-open

# Install CUDA 
sudo apt-get -y install --no-install-recommends cuda-12-8

# Add PATH
grep CUDA ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> ~/.bash_aliases
  echo '# CUDA' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}' >> ~/.bash_aliases
fi

exit

