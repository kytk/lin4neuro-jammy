#!/bin/bash

# Lin4Neuro building script for Ubuntu 22.04 (Jammy)
# Part 1: Setup Lin4Neuro
# This script installs XFCE and Lin4Neuro theme.
# Pre-requisite: You need to install Ubuntu mini.iso or standard Ubuntu and git beforehand.

# 21 Feb 2025 K. Nemoto

#(optional) Force IPv4
#Uncomment the following two lines if you want the system to use only IPv4
#echo 'Acquire::ForceIPv4 "true";' | sudo tee /etc/apt/apt.conf.d/99force-ipv4
#echo '--inet4-only=1' >> ~/.wgetrc

# Function to display usage information
usage() {
    echo "Usage: $0 [OPTION]"
    echo "Options:"
    echo "  -l, --lang LANG    Specify language (en/ja)"
    echo "  -h, --help         Display this help message"
    echo ""
    echo "Examples:"
    echo "  $0 -l en    # Install with English language"
    echo "  $0 -l ja    # Install with Japanese language"
    echo "  $0          # Interactive language selection"
    exit 1
}

# Function to select language interactively
select_language_interactive() {
    echo "Which language do you want to build? (English/Japanese)"
    select lang in "English" "Japanese" "quit"
    do
        if [ "$REPLY" = "q" ] ; then
            echo "quit."
            exit 0
        fi
        if [ -z "$lang" ] ; then
            continue
        elif [ $lang == "Japanese" ] ; then
            SELECTED_LANG="Japanese"
            break
        elif [ $lang == "English" ] ; then
            SELECTED_LANG="English"
            break
        elif [ $lang == "quit" ] ; then
            echo "quit."
            exit 0
        fi
    done
}

# Parse command line arguments
SELECTED_LANG=""
while [[ $# -gt 0 ]]; do
    case $1 in
        -l|--lang)
            if [[ $2 == "en" ]]; then
                SELECTED_LANG="English"
            elif [[ $2 == "ja" ]]; then
                SELECTED_LANG="Japanese"
            else
                echo "Error: Unknown language option: $2"
                usage
            fi
            shift 2
            ;;
        -h|--help)
            usage
            ;;
        *)
            echo "Unknown option: $1"
            usage
            ;;
    esac
done

##### STEP 0. PREPARATION ######################################################

# Update system first
LANG=C
sudo apt-get update; sudo apt-get upgrade -y
sudo apt-get install -y wget gnupg

# Uncomment the following line to install hwe kernel
# linux-{image,headers}-generic-hwe-22.04
sudo apt-get install -y linux-{image,headers}-generic-hwe-22.04

# disable screensaver
xset s off
xset s noblank

# make sure the default linux-generic is installed
sudo apt-get install -y linux-{image,headers}-generic --reinstall

# Log
log=$(date +%Y%m%d%H%M%S)-vm1.log
exec &> >(tee -a "$log")

# Set language environments
echo "Begin making Lin4Neuro."

# If no language was selected via command line, use interactive method
if [ -z "$SELECTED_LANG" ]; then
    select_language_interactive
fi

echo "Selected language: $SELECTED_LANG"

# Configure system based on selected language
if [ "$SELECTED_LANG" == "Japanese" ]; then
    #Setup Japanese locale
    sudo apt-get install -y language-pack-ja manpages-ja
    sudo locale-gen ja_JP.UTF-8
    sudo update-locale LANG=ja_JP.UTF-8

    #replace us with ja in  /etc/apt/sources.list
    sudo sed -i 's|http://us.|http://jp.|g' /etc/apt/sources.list
    sudo apt-get update

elif [ "$SELECTED_LANG" == "English" ]; then
    #Setup English locale
    sudo apt-get install -y language-pack-en
    sudo locale-gen en_US.UTF-8
    sudo update-locale LANG=en_US.UTF-8
fi


##### STEP 1. Install XFCE and utilities #######################################

# Remove gdm3 and wayland
#sudo apt-get -y purge gdm3 xwayland

# Install XFCE
echo "Install XFCE"
sudo apt-get install -y xfce4 xfce4-terminal xfce4-indicator-plugin  \
 xfce4-clipman xfce4-clipman-plugin xfce4-statusnotifier-plugin  \
 xfce4-power-manager-plugins xfce4-screenshooter \
 lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings \
 shimmer-themes xinit build-essential  \
 dkms thunar-archive-plugin file-roller gawk xdg-utils 



##### STEP 2. INSTALL LIN4NEURO ENVIRONMENTS ###################################

# Path of the lin4neuro parts
base_path=$PWD/lin4neuro-parts

# Plymouth-related files
sudo apt-get install -y plymouth-themes plymouth-label

# Lin4neuro-logo
sudo cp -r "${base_path}"/lin4neuro-logo /usr/share/plymouth/themes
sudo update-alternatives --install      \
 /usr/share/plymouth/themes/default.plymouth    \
 default.plymouth       \
 /usr/share/plymouth/themes/lin4neuro-logo/lin4neuro-logo.plymouth \
 120
sudo update-initramfs -u -k all

# Icons
mkdir -p ~/.local/share
cp -r "${base_path}"/local/share/icons ~/.local/share/
sudo mkdir -p /etc/skel/.local/share
sudo cp -r "${base_path}"/local/share/icons /etc/skel/.local/share/

# Customized menu
mkdir -p ~/.config/menus
cp "${base_path}"/config/menus/xfce-applications.menu ~/.config/menus
sudo mkdir -p /etc/skel/.config/menus
sudo cp "${base_path}"/config/menus/xfce-applications.menu \
 /etc/skel/.config/menus

# Cusotmized panel, dsktop, and theme
cp -r "${base_path}"/config/xfce4 ~/.config
sudo cp -r "${base_path}"/config/xfce4 /etc/skel/.config

# Desktop files
cp -r "${base_path}"/local/share/applications ~/.local/share/
sudo cp -r "${base_path}"/local/share/applications /etc/skel/.local/share/

# Neuroimaging.directory
mkdir -p ~/.local/share/desktop-directories
cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory ~/.local/share/desktop-directories
sudo mkdir -p /etc/skel/.local/share/desktop-directories
sudo cp "${base_path}"/local/share/desktop-directories/Neuroimaging.directory /etc/skel/.local/share/desktop-directories

# Background image (and remove unnecessary image file)
cd /usr/share/backgrounds/xfce
sudo rm *.jpg *.png
sudo cp "${base_path}"/backgrounds/deep_ocean.png /usr/share/backgrounds/xfce/

# Modified lightdm-gtk-greeter.conf
#sudo mkdir -p /usr/share/lightdm/lightdm-gtk-greeter.conf.d
#sudo cp "${base_path}"/lightdm/lightdm-gtk-greeter.conf.d/01_ubuntu.conf /usr/share/lightdm/lightdm-gtk-greeter.conf.d
sudo cp "${base_path}"/lightdm/lightdm-gtk-greeter.conf /etc/lightdm

# Auto-login
#sudo mkdir -p /usr/share/lightdm/lightdm.conf.d
#sudo cp "${base_path}"/lightdm/lightdm.conf.d/10-ubuntu.conf \
# /usr/share/lightdm/lightdm.conf.d
sudo cp "${base_path}"/lightdm/lightdm.conf.d/50-xfce-session.conf \
 /etc/lightdm/lightdm.conf.d

# Network Manager
sudo cp "${base_path}"/etc/NetworkManager/conf.d/10-globally-managed-devices.conf /etc/NetworkManager/conf.d/10-globally-managed-devices.conf

# Clean packages
sudo apt-get -y autoremove

# GRUB customization to show GRUB on boot
sudo sed -i -e 's/GRUB_TIMEOUT_STYLE/#GRUB_TIMEOUT_STYLE/' /etc/default/grub
sudo sed -i -e 's/GRUB_TIMEOUT=0/GRUB_TIMEOUT=10/' /etc/default/grub
sudo update-grub

# Boot repair
sudo add-apt-repository -y ppa:yannubuntu/boot-repair
sudo apt-get install -y boot-repair

# alias
cat << EOS >> ~/.bash_aliases

#alias for xdg-open
alias open='xdg-open &> /dev/null'

EOS

# copy .bash_aliases to /etc/skel
sudo cp ~/.bash_aliases /etc/skel/

# Deactivate screensaver
echo 'xset s off' >> ~/.xsession

#### Ubuntu to Lin4Neuro (if Ubuntu is installed beforehand)
# Delete ubuntu-desktop
sudo apt-get purge -y nautilus gnome-power-manager \
  gnome-bluetooth gnome-desktop* gnome-session* \
  gnome-user* gnome-shell-common gnome-screenshot*
sudo apt-get install -y gnome-software gnome-system-tools eog evince
sudo apt-get purge -y gnome-terminal* tracker tracker-extract tracker-miner-fs
 
# Clean packages
sudo apt-get -y autoremove

# Remove unnecessary configuration files
dpkg -l | awk '/^rc/ {print $2}' | xargs sudo dpkg --purge

# locale-related settings
if [ "$SELECTED_LANG" == "Japanese" ] ; then
  sed -i -e 's/デスクトップ/Desktop/' -e 's/ダウンロード/Downloads/' \
    -e 's/テンプレート/Templates/' -e 's/公開/Public/' \
    -e 's/ドキュメント/Documents/' -e 's/ミュージック/Music/' \
    -e 's/ピクチャ/Pictures/' -e 's/ビデオ/Videos/' ~/.config/user-dirs.dirs
  echo "ja_JP" > ~/.config/user-dirs.locale
  cd $HOME
  rmdir ダウンロード テンプレート デスクトップ ドキュメント \
        ビデオ ピクチャ ミュージック 公開
fi
####

##### Install utilities #####

# Install network-manager-gnome
sudo apt-get install -y --no-install-recommends network-manager-gnome 

# Install python-related packages
echo "Install python libraries"
sudo apt-get install -y \
  python3-pip python3-venv python3-dev python3-tk \
  python3-gpg pkg-config libopenblas-dev liblapack-dev  \
  libhdf5-serial-dev graphviz 

# Install python libraries under ~/myvenv
# requirements.txt
cat << EOS > requirements.txt
bash_kernel
beautifulsoup4
cmake
cycler
dcm2bids
gdcm
h5py
heudiconv
jupyter
kiwisolver
matplotlib
nibabel
nilearn
notebook
numpy
opencv-python
openpyxl
packaging
pandas
pillow
pydicom
pydot-ng
python-dateutil
pytz
pyyaml
scikit-learn
scipy
seaborn
EOS

#/usr/bin/python3 -m venv myvenv
#source ~/myvenv/bin/activate
python3 -m pip install -U pip
pip install -r requirements.txt
python3 -m bash_kernel.install
#deactivate

#echo >> ~/.bash_aliases
#echo '# Activate python venv under ~/myvenv' >> ~/.bash_aliases
#echo 'source ~/myvenv/bin/activate' >> ~/.bash_aliases

# Install utilities
sudo apt-get install -y at-spi2-core bc byobu curl wget dc \
 default-jre evince exfatprogs gedit mousepad \
 gnome-system-monitor gnome-system-tools gparted  \
 imagemagick rename ntp system-config-printer  \
 tree unzip update-manager vim alsa-base wajig zip ntp tcsh baobab xterm \
 bleachbit libopenblas-base cups apturl dmz-cursor-theme \
 chntpw gddrescue p7zip-full gnupg eog meld \
 software-properties-common fonts-noto mupdf mupdf-tools pigz \
 ristretto pinta nfs-common

# Remove snapd
sudo apt-get -y purge snapd

# Workaround for system-config-samba
sudo touch /etc/libuser.conf
# Vim settings
cp /usr/share/vim/vimrc ~/.vimrc
sed -i -e 's/"set background=dark/set background=dark/' ~/.vimrc

# Install libreoffice with language locale
if [ "$SELECTED_LANG" == "English" ] ; then
  #English-dependent packages
  echo "Installation of libreoffice"
  sudo apt-get install -y libreoffice libreoffice-help-en-us
elif [ "$SELECTED_LANG" == "Japanese" ] ; then
  #Japanese-dependent environment
  echo "Installation of Japanese-related packages"
  sudo apt-get install -y fcitx fcitx-mozc  \
              unar nkf im-config language-pack-ja language-pack-gnome-ja 
  echo "Installation of libreoffice"
  sudo apt-get install -y libreoffice libreoffice-l10n-ja \
              libreoffice-help-ja
  # Change name of directories to English
  LANG=C xdg-user-dirs-update --force
  im-config -n fcitx
fi

# Install Firefox without snap
# based on https://support.mozilla.org/kb/install-firefox-linux
wget -q https://packages.mozilla.org/apt/repo-signing-key.gpg -O- |\
  sudo tee /etc/apt/keyrings/packages.mozilla.org.asc > /dev/null 
echo "deb [signed-by=/etc/apt/keyrings/packages.mozilla.org.asc] https://packages.mozilla.org/apt mozilla main" |\
  sudo tee -a /etc/apt/sources.list.d/mozilla.list
echo -e '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' | sudo tee /etc/apt/preferences.d/mozilla
sudo apt-get update && sudo apt-get install -y firefox

# Install Google-chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install -y ./google-chrome-stable_current_amd64.deb
sleep 3
rm google-chrome-stable_current_amd64.deb

# set google chrome as default for xdg-mime
xdg-mime default google-chrome.desktop text/html

# (optional) latest Libreoffice
#Uncomment the following three lines to use the latest libreoffice
sudo add-apt-repository -y ppa:libreoffice/ppa
sudo apt-get update
sudo apt-get -y dist-upgrade

# clamAV
sudo apt-get -y install clamav clamav-daemon 


# ~/bin
mkdir -p ~/bin

echo "Finished!"
echo "Please run l4n-jammy-2.sh to install neuroimaging packages after reboot."

#sleep 10

#echo "reboot"
#reboot
